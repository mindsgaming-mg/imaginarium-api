# Imaginarium API

Create your own VR rooms with the [Imaginarum Cloud](https://gitlab.com/mindsgaming-mg/imaginarium-cloud) backend.

![](https://cdn.glitch.com/eb1b4295-0ee9-4132-98b3-29f39dc80c9f%2Fgitimage.png?v=1615246625090)

**Website:** [https://imaginarium-api.glitch.me/](https://imaginarium-api.glitch.me/)

---

## You Can Clone Or Remix This Project it runs as an HTML webpage

### Clone

<code>git clone https://gitlab.com/mindsgaming-mg/imaginarium-api.git</code>

- Open index in a browser LP

## Remix On Glitch

Remix this project on Glitch and have a website up in seconds

[Remix](https://glitch.com/edit/#!/remix/imaginarium-api)

#### Scripts

You can also just add our scripts to your html <code>head</code>

<code>
    <script src="https://imaginarium-api.glitch.me/script.js" defer></script>
    <script src="https://imaginarium-cloud.glitch.me/Imaginarium/script.js"></script>
    <script src="https://imaginarium-cloud.glitch.me/https.js"></script>
    <script src="https://aframe.io/releases/1.2.0/aframe.min.js"></script>
    <script src="https://imaginarium-cloud.glitch.me/dist/aframe-master.js"></script>
    <script src="https://imaginarium-cloud.glitch.me/vr/page-controls.js"></script>
</code>

---

## Project Files

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

Where you'll write the content of your website.

### ← style.css

CSS files add styling rules to your content.

### ← script.js

If you're feeling fancy you can add interactivity to your site with JavaScript.

### ← assets

Drag in `assets`, like images or music, to add them to your project

## Made with Imagination Built On [Glitch](https://glitch.com/~imaginarium-api)

\ ゜ o ゜)ノ
